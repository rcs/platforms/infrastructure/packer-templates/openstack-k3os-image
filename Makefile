.PHONY: help prepare clean validate build terraform

help:
	@echo "Hi there!"
	@echo "Let's build some k3os in Openstack"
	@echo ""
	@echo "Our options are ::"
	@echo "prepare:       will clone the k3os repository. We need this."
	@echo "terraform:     creates essential openstack infra."
	@echo "validate:      validate the packer template"
	@echo "build:         build the image"
	@echo "clean:         remove the k3os directory"

# Check requirements
ifeq (, $(shell which packer))
    $(error Error! packer command not in PATH)
endif
ifeq (, $(shell which openstack))
    $(error Error! openstack command not in PATH. Check readme to set this up)
endif

prepare:
	@git clone https://github.com/rancher/k3os.git

validate:
	@packer validate templates/k3os-template.json

build: validate
	@packer build templates/k3os-template.json

clean:
	@rm -rf k3os

terraform:
	$(MAKE) -C terraform init
	$(MAKE) -C terraform plan
	$(MAKE) -C terraform apply

output:
	$(MAKE) -C terraform output