variable "dns_nameservers" {
  type        = "list"
  default     = ["131.111.8.42", "131.111.12.20"]
  description = "CUDN DNS Nameservers"
}

variable "public_network" {
  default     = "d579d662-47f0-4b78-bf4f-030fe07fe477"
  description = "UUID of the external network"
}

variable "enable_gateway" {
  type = "string"

  # yes/no
  default     = "yes"
  description = " Enable gateway on the subnet created in this network."
}
