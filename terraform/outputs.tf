output "packer_network_id" {
  value       = ["${openstack_networking_network_v2.packer-internal-net.id}"]
  description = "Packer network ID"
}
