# Openstack k3os Packer Image

## what is this?
[k3os](https://github.com/rancher/k3os) is a kubernetes first Operating System by Rancher. 

This packer template allows you to create images for k3os inside Research Computing Service's private cloud. 

[Contact us](https://www.hpc.cam.ac.uk/) if you wish to get access to a testing tenancy to try this out. 

## Prereqs

* [ ] python-Openstack

You'll need the python package `python-openstack` installed and on your path. 
We've included a pipfile, so if you have python 3.7.x and pipenv available you may do a `pipenv --python 3.7.x` followed by `pipenv install`

* [ ] packer 1.4.5 - Needs to be available on your path

* [ ] Openstack tenancy with bare essentials configured. We've included some Terraform here to make this bit quick.
* [ ] If using Terraform then you'll want Terraform >0.12 available on your path. Recommend the excellent [TFSwitch](https://warrensbox.github.io/terraform-switcher/).

## Let's do this

> All commands are run from the root of this project. 

Source our Openstack env variables. You may get a copy from Horizon UI or edit the one in `env/openstack.sh`: `source env/openstack.sh`

Check we can connect to Openstack via the Python Client:
`Openstack image list`

Make the bear necessities in Openstack:
`make terraform`

Once you have those pull the network ID Packer will use:
`make output`

That needs to got into `./env/vars.sh`, replacing the UUID on this line:

`export OS_NETWORKS_ID="6b965c7e-ba04-4d6a-bd78-33173f8ccee8"`

After updating vars.sh we need to source it: `source env/vars.sh`

Building the image:
```
make prepare
make validate
make build
```
Once done, check out your new image:

`openstack image list`